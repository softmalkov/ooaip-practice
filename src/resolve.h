#pragma once

#include <string>

#include "IUObject.h"

template <typename T>
T resolve(const std::string &key, IUObject &obj)
{
    return std::any_cast<T>(obj[key]);
}

template <typename T>
void resolve(const std::string &key, IUObject &obj, T val)
{
    obj[key] = val;
}