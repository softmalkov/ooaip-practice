#include "gtest/gtest.h"

#include "IUObject.h"
#include "interfaces.h"
#include "resolve.h"
#include "commands.h"

TEST(burnFuelCommand, normal)
{
    UObject *tank = new UObject();
    FuelableAdapter<int> *fuelable = new FuelableAdapter<int>(*tank);
    Command *bf_command = new BurnFuelCommand(*fuelable, 10);

    
    fuelable->setFuelLevel(100);
    ASSERT_EQ(fuelable->getFuelLevel(), 100);
    bf_command->Execute();
    ASSERT_EQ(fuelable->getFuelLevel(), 90);

/*
    ASSERT_EQ(fuelable->getFuelLevel(), 100)

    fuelable->setFuelLevel(90);

    ASSERT_EQ(fuelable->getFuelLevel(), 90)*/
}